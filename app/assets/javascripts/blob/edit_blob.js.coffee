class @EditBlob
  constructor: (assets_path, ace_mode = null) ->
    ace.config.set "modePath", "#{assets_path}/ace"
    ace.config.loadModule "ace/ext/searchbox"
    @editor = ace.edit("editor")
    @editor.focus()
    @editor.getSession().setMode "ace/mode/#{ace_mode}" if ace_mode

    # Before a form submission, move the content from the Ace editor into the
    # submitted textarea
    $('form').submit =>
      $("#file-content").val(@editor.getValue())

    @initModePanesAndLinks()
    new BlobLicenseSelector(@editor)

  initModePanesAndLinks: ->
    @$editModePanes = $(".js-edit-mode-pane")
    @$editModeLinks = $(".js-edit-mode a")
    @$editModeLinks.click @editModeLinkClickHandler

    promptUser = false

    # Confirm changes should be discarded
    @editor.on "change", =>
      if not promptUser
        promptUser = true
        $(document).on 'page:before-change', (e) =>
          @confirmDiscardChanges(e, false)
        $(window).bind 'beforeunload', (e) =>
          @confirmDiscardChanges(e)
        # Don't prompt user when they click commit || cancel
        $('.form-actions a, button').click ->
          $(document).off 'page:before-change'
          $(window).unbind 'beforeunload'

  editModeLinkClickHandler: (event) =>
    event.preventDefault()
    currentLink = $(event.target)
    paneId = currentLink.attr("href")
    currentPane = @$editModePanes.filter(paneId)
    @$editModeLinks.parent().removeClass "active hover"
    currentLink.parent().addClass "active hover"
    @$editModePanes.hide()
    currentPane.fadeIn 200
    if paneId is "#preview"
      $.post currentLink.data("preview-url"),
        content: @editor.getValue()
      , (response) ->
        currentPane.empty().append response
        currentPane.syntaxHighlight()

    else
      @editor.focus()

  confirmDiscardChanges: (e, isPageRefresh=true) ->
    msg = "Discard Changes?";
    if isPageRefresh
      return msg
    else
      if not confirm msg
        e.preventDefault()
        e.cancelBubble = true
      